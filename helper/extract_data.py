#! /usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd

FNAME = "leipzig_holzhausen.json"
VAR = "TT_10"


if __name__ == "__main__":
    df = pd.read_json(FNAME)
    timeseries = pd.DataFrame(df.timeseries.to_list())[["timestamp", "values"]]
    var_values = []
    for _, (ts, values) in timeseries.iterrows():
        v = [v["value"] for v in values if v["name"] == VAR]
        var_values.append(v[0])
    df = pd.DataFrame(
        data={VAR: var_values},
        index=pd.DatetimeIndex(timeseries["timestamp"]))

    selection = df.loc[(df.index >= "2019-04-01") & (df.index < "2019-04-02")]
    selection.to_csv("test.csv",
                     index=True,
                     index_label="date",
                     date_format='%Y-%m-%d %H:%M:%S',
                     header=["data"])
